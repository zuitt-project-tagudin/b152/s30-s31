const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;
mongoose.connect("mongodb+srv://mongoadmin:admin@cluster0.sao0e.mongodb.net/task152?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}); //mongoose connection


let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error.'));
db.once('open', ()=> console.log("Connected to MongDB"));


app.use(express.json()); //IMPORTANT
//express.json() handles the request body of the request/ it handles JSON data from client


const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes); //middleware groouping all routes under /tasks


const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

app.listen(port, ()=> console.log(`running on ${port}`));