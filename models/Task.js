const mongoose = require("mongoose");


//Schema acts as a blueprint for the document
const taskSchema = new mongoose.Schema({

	name: String,
	status: String,

});





/*
	Mongoose Model

	used to connect api to corresponding collection in database. it is a representation of the Task documents

	models use schemas to create objects that correspond to the schema. by default, when creating the collection for the model, the collection name is pluralized.

	mongoose.mode(nameofCollectionInAtlas. schemaToFollow)
*/

module.exports = mongoose.model("Task", taskSchema);