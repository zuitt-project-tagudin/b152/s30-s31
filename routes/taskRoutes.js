const express = require('express');
const router = express.Router();
const taskControllers = require("../controllers/taskControllers")
/*Router() is method from express that allows acces to http method routes; acts as middleware and as a routing system*/

router.post('/', taskControllers.createTaskController);

router.get('/', taskControllers.getAllTasksController);

router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController);

module.exports = router;