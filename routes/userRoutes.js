const express = require("express");
const router = express.Router();

const userControllers = require('../controllers/userControllers');
//ACTIVITY 

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

router.put('/updateName/:id', userControllers.updateNameController);

module.exports = router;