const Task = require('../models/Task');



module.exports.createTaskController = (req, res) => {

	Task.findOne({name: req.body.name})
	.then(result => {

		if (result !== null && result.name === req.body.name) {
			return res.send("task with same name")
		} else {

			let newTask = new Task({
				name: req.body.name,
				status: req.body.status,
			})


			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}

	})
	.catch(error => res.send(error));

}

module.exports.getAllTasksController = (req, res) => {

	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
	
}

module.exports.getSingleTaskController = (req, res) => {
	
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}

module.exports.updateTaskStatusController = (req, res) => {
	let id = req.params.id;
	console.log(id);
	console.log(req.body.status);

	let updates = {
		status: req.body.status
	}

	Task.findByIdAndUpdate(id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
}

//the third argument {new: true}, returns the updated version of the document. by default, with out the argument, .findIdAndUpdate() will return the previous state of the document.