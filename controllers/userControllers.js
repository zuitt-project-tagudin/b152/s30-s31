const User = require('../models/User');

module.exports.createUserController = (req, res) => {

	User.findOne({username: req.body.username})
	.then(result => {
		if(result !== null && result.username === req.body.username) {
			
			return res.send("Username already in use")

		} else {

			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})
	.catch(error => res.send(error));

}

module.exports.getAllUsersController = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}

module.exports.getSingleUserController = (req, res) => {
	console.log(req.params.id);
	let id = req.params.id;

	User.findById(id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.updateNameController = (req, res) => {

	let id = req.params.id;
	let updates = {
		username: req.body.username
	}

	User.findByIdAndUpdate(id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))
}